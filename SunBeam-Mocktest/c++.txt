1. What is the output of the following code?

int num1 = 10;
namespace Outer
{
	int num1 = 20;
}
int main( void )
{
	int num1 = 30;
	using namespace Outer;
	cout<<"Num1	:	"<<num1<<endl;
	return 0;
}
2. #include<iostream>
using namespace std;
#define INC(X) X++
int main()
{
int X = 4;
cout<<INC( X++ ) <<endl;
return 0;
}
3. What is the output of the following code?

class Program
{
public:
	void print( int num1 )
	{
		cout<<"Instance:"<<num1<<endl;
	}
	static void print( int num1 )
	{
		cout<<"static:"<<num1<<endl;
	}
};
int main( void )
{
	Test test;
	test.print(10);
}
4. What will be the output of the following program?

int main( void )
{
	int num1 = 10;
	int& num2 = num1;
	num2 = num1 ++;
	num1 = num2 ++;
	cout<<num1<<"	"<<num2<<endl;
	return 0;
}
5. If X is name of the class then what is the correct way to declare copy constructor of X?
6. Which one of the following statements is not true about destructor?
7. Which one of the following does not inherit into the derived class?
8. ___________ is the process of compartmentalizing the elements of an abstraction that constitute its structure and behavior?
9. What is the output of the following program?


class Base
{
private:
	int num1;
private:
	Base( int num1 = 0)
	{
		this->num1 = num1;
	}
public:
	void print( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;
	}
	friend class Derived;
};
class Derived: public Base
{
private:
	int num2;
public:
	Derived( int num1, int num2 ) : Base( num2 )
	{
		this->num2 = num2;
	}
	void print( void )
	{
		Base::print();
		cout<<"Num2	:	"<<this->num2<<endl;
	}
};
int main( void )
{
	Derived derived( 10, 20 );
	derived.print();
	return 0;
}
10. Which one of the following operators can not be overloaded as a non-member function?
11. Find the output of the following program?

class Base
{
public:
	virtual void print( void )
	{
		cout<<"Base::print"<<endl;
	}
};
class Derived : Base
{
public:
	virtual void print( void )
	{
		cout<<"Derived::print"<<endl;
	}
};
int main( void )
{
	Base* ptrBase = new Derived();
	ptrBase->print();
	delete ptrBase;
	return 0;
}
12. Observe the code and choose the correct option:

class Base
{
public:
	Base()
	{	}
	virtual void print( void )
	{
		cout<<"Base::print"<<endl;
	}
};
class Derived: public Base
{
public:
	Derived()
	{
		this->print();
	}
	virtual void print( void )
	{
		cout<<"Derived::print"<<endl;
	}
};
int main( void )
{
	Derived derived;
	return 0;
}
13. What is the output of the following code?

int calculate( int num1, int num2 )throw(const char*)
{
	if( num2 == 0 )
		throw string("Divide by zero exception");
	return num1 / num2;
}
int main( void )
{
	try
	{
		int result = ::calculate(10,0);
	}
	catch( const char* ex )
	{
		cout<<"const char*"<<endl;
	}
	catch( string& ex )
	{
		cout<<"string"<<endl;
	}
	catch(...)
	{
		cout<<"Divide by zero exception"<<endl;
	}
}
14. Which of the following line will give a compiler error?

class Base
{	};
class Derived : virtual public Base	//Line 1
{	};
int main( void )
{
	Base* ptrBase = new Base;	//Line 2
	Derived* ptrDerived1 = dynamic_cast<Derived*>( ptrBase ); 	//Line 3
	Derived* ptrDerived2 = reinterpret_cast<Derived*>( ptrBase);	//Line 4
	return 0;
}
15. To find out the true type of object, If we use the null pointer with typeid then output is ____
16. Which stream class is used to perform read as well as write operations on file?
17. Which one of the following STL container store elements in adjacent memory locations?
18. Which one of the following is not a fundamental datatype in C++?
19. We can convert pointer of child type into pointer of parent type. It is called _______________?
20. What is the output of the following code?

class A
{
private:
	class B
	{
	private:
		int number;
	public:
		B( void ) : number( 10 ){	}
		friend class A;
	};
public:
	class C
	{
	public:
		void print( void )
		{
			B obj;
			cout<<"Number	:	"<<obj.number<<endl;
		}
	};
};
int main( void )
{
	A::C obj;
	obj.print();
	return 0;
}
21. by default all data members of class are _______ and all data members of struct are____.
22. State of object can be modified in ---------------?
23. Which of the following way is correct to access static data member with class name?
24. If you want to modify data member inside a constant member function, the data member should be declared as --------.
25. In c plus plus programming language we can initialized pointer to _____. 
	In c plus plus programming language we can not initialized reference to ____.
26. Which of the following is/are valid ways to allocate memory for an integer by dynamic memory allocation in c plus plus?
27. Which of the following is true statement about new in cpp ?
28. #include<iostream>
using namespace std;
int main(void)
{
	enum colors{ RED,BLUE=-1,GREEN,YELLOW=-1 };
	cout<<YELLOW<<" "<<GREEN<<" "<<BLUE<<" "<<RED<<endl;
	return 0;
}
29. #include<iostream>
using namespace std;
class democlass
{
    char ch;
	public:
    democlass(char x){
    	this->ch = ch;
	}
    democlass(const democlass p) {
    	this->ch = p.ch;
    }
    char getch() { return ch; }
};
int main()
{
	democlass objInstance1('A');
	democlass objInstance2 = objInstance2;
	cout << objInstance1.getch();
	return 0;
}
30. #include<iostream>
using namespace std;
class democlass
{
	public:
		democlass()
		{
    		cout << "Constructor called "<<endl;
		}
		democlass(const democlass &t)
    	{
    		cout << "Copy constructor called"<<endl;
    	}
};
int main()
{
	democlass *t1=NULL, *t2=NULL;
	t1 = new democlass();
	t2 = new democlass(*t1);
	democlass t3 = *t1;
	democlass t4;
	t4 = t3;
	return 0;
}
31. In c++ by default mode of inheritance is ____________.
32. in diamond problem if we consider class A,B,C,D then constructor calling sequence for following code.
class A {};
class B: virtual public A {};
class C: virtual public A {};
class D: public C, public B {};
33. function overloading is --------- and function overriding is --------.
34. to make member function Constant in C++ which the correct way of following
35. if you want to do type conversion between incompatible types then we should use ------- operator.
36. ___________ can occur with in same class or same scope( in global functions) and _________ occurs when one class is inherited from another class.
37. A __________ is a  member function that is declared within a base class and it is not compulsory to  redefined by a derived class. and  __________ is a  member function that is declared within a base class and it is compulsory to  redefined by a derived class.
38. when object of any user defined class having dynamic memory allocation in constructor of class is thrown by exception handling. you should throw it by ____ as argument to avoid extra functions calls.
39. Which of the following are the default standard streams in C++.

1. cin
2 cout
3 cerr 
4 clog
40. To delete all content of file while opening it, which mode is use ______.

