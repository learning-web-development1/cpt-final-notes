/* 
- Name: megamenu.js - style.css
- Version: 1.0
- Latest update: 29.01.2016.
- Author: Mario Loncarek
- Author web site: http://marioloncarek.com
*/


/* ––––––––––––––––––––––––––––––––––––––––––––––––––
Body - not related to megamenu
–––––––––––––––––––––––––––––––––––––––––––––––––– */

body {
    font-family: 'Open Sans', sans-serif;
    font-size:1.25em;
}

* {
    box-sizing: border-box;
}

a.actscolor {
    color: #000000;
	
}

.description {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    -webkit-transform: translateX(-50%);
    -ms-transform: translateX(-50%);
    transform: translateX(-50%);
}


/* ––––––––––––––––––––––––––––––––––––––––––––––––––
megamenu.js STYLE STARTS HERE
–––––––––––––––––––––––––––––––––––––––––––––––––– */


/* ––––––––––––––––––––––––––––––––––––––––––––––––––
Screen style's
–––––––––––––––––––––––––––––––––––––––––––––––––– */

.menu-container {
    width: 100%;
    margin: 0 auto;
   background-image:url('/index.aspx?id=edu_acts_Strip_2');
    background-repeat:repeat-x;
	 
	
}

.menu-mobile {
    display: none;
    padding: 10px;
}

/*.menu-mobile:after {
    content: "\f394";
    font-family: "Ionicons";
    font-size: 2.5rem;
    padding: 0;
    float: right;
    position: relative;
    top: 50%;
    -webkit-transform: translateY(-25%);
    -ms-transform: translateY(-25%);
    transform: translateY(-25%);
}

.menu-dropdown-icon:before {
    content: "\f489";
    font-family: "Ionicons";
    display: none;
    cursor: pointer;
    float: right;
    padding: 1.5em 2em;
    background: #fff;
    color: #333;
}*/

.menu > ul {
    margin: 0 auto;
    width: 900px;
    list-style: none;
    padding: 0;
    position: relative;
    /* IF .menu position=relative -> ul = container width, ELSE ul = 100% width */
    box-sizing: border-box;
}

.menu > ul:before,
.menu > ul:after {
    content: "";
    display:table;
}

.menu > ul:after {
    clear: both;
}

.menu > ul > li {
    float: left;
     
    padding: 0;
    margin: 0;
}

.menu > ul > li a {
    text-decoration: none;
    padding: 1em 1.6em;
    display: block;
     margin-right:0px;
}

.menu > ul > li a:hover {
    text-decoration: none;
    display: block;
     background-color:#FFAB55;
     border-radius:0px;
     margin-right:0px;
     
}

.menu > ul > li > ul > li:hover {
    text-decoration: none;
    padding: 1em 2em;
    display: block;
     background-color:#FFAB55;
}

.menu > ul > li:hover {
    background: #FFC082;
}

.menu > ul > li > ul {
    display: none;
    width: 100%;
    background: #FFC082;
    padding: 5px;
    position: absolute;
    z-index: 99;
    left: 0;
    margin: 0;
    list-style: none;
    box-sizing: border-box;
}

 

.menu > ul > li > ul:before,
.menu > ul > li > ul:after {
    content: "";
    display: table;
}

.menu > ul > li > ul:after {
    clear: both;
}

.menu > ul > li > ul > li {
    margin: 0;
    padding-bottom: 0;
    list-style:none;
    width: 25%;
    background: none;
    float: left;
    
}
 
 .menu > ul > li > ul > li a:hover 
 {
 	 background-color:#ff9934;
  color:#000;
  padding: .2em .3em .2em .2em;
    width: 95%;
    display: block;
	border-bottom: 0px solid #ccc;
	border-right:2px solid #ccc;
	font-size:12px;
}

 
.menu > ul > li > ul > li a {
    color: #525252;
  padding: .2em .3em .2em .2em;
    width: 95%;
    display: block;
	border-bottom: 0px solid #ccc;
	border-right:2px solid #ccc;
	font-size:12px;
}




.menu > ul > li > ul > li > ul {
    display: block;
    padding: 0;
    margin: 8px 0 0;
    list-style: none;
    box-sizing: border-box;
}

.menu > ul > li > ul > li > ul:before,
.menu > ul > li > ul > li > ul:after {
    content: "";
    display: table;
}

.menu > ul > li > ul > li > ul:after {
    clear: both;
}

.menu > ul > li > ul > li > ul > li {
    float: left;
    width: 100%;
    padding: 3px 0;
    margin: 0;
    font-size: 85%;
}

.menu > ul > li > ul > li > ul > li a {
    border: 0;
}
.menu > ul > li > ul > li > ul > li a {
    border: 0;
}

.menu > ul > li > ul.normal-sub {
    width: 300px;
    left: auto;
    padding: 10px 20px;
}

.menu > ul > li > ul.normal-sub > li {
    width: 100%;
}

.menu > ul > li > ul.normal-sub > li a {
    border: 0;
    padding: 1em 0;
}


/* ––––––––––––––––––––––––––––––––––––––––––––––––––
Mobile style's
–––––––––––––––––––––––––––––––––––––––––––––––––– */

@media only screen and (max-width: 767px) {
    .menu-container {
        width: 100%;
        
    }
    .menu-mobile {
        display: block;
    }
    .menu-dropdown-icon:before {
        display: block;
    }
    .menu > ul {
        display: none;
         width: 100%;
    }
    .menu > ul > li {
        width: 100%;
        float: none;
        display: block;
    }
    .menu > ul > li a {
        padding: 1.5em;
        width: 100%;
        display: block;
    }
    .menu > ul > li > ul {
        position: relative;
    }
    .menu > ul > li > ul.normal-sub {
        width: 100%;
    }
    
    .menu > ul > li > ul.normal-sub > li a {
        width: 100%;
    }
    .menu > ul > li > ul > li {
        float: none;
        width: 100%;
        margin-top: 20px;
    }
    .menu > ul > li > ul > li:first-child {
        margin: 0;
    }
    .menu > ul > li > ul > li > ul {
        position: relative;
    }
    .menu > ul > li > ul > li > ul > li {
        float: none;
    }
    .menu .show-on-mobile {
        display: block;
    }
}

/* General Styles for Menu  */
.menuBackground {
	background: brown;
	text-align: center;
}
.dropDownMenu a {
	color: #FFF;
}
.dropDownMenu,
.dropDownMenu ul {
	list-style: none;
	margin: 0;
	padding: 0;
}
.dropDownMenu li {
	position: relative;
}
.dropDownMenu a {
	padding: 10px 20px;
	display: block;
	text-decoration: none;
}
.dropDownMenu a:hover {
	background: #000;
}


/* Level 1 Drop Down */
.dropDownMenu > li {
	display: inline-block;
	vertical-align: top;
	margin-left: -4px; /* solve the 4 pixels spacing between list-items */
}
.dropDownMenu > li:first-child {
	margin-left: 0;
}

/* Level 2 */
.dropDownMenu ul {
	box-shadow: 2px 2px 15px 0 rgba(0,0,0, 0.5);
}
.dropDownMenu > li > ul {
	text-align: left;
	display: none;
	background: green;
	position: absolute;
	top: 100%;
	left: 0;
	width: 240px;
	z-index: 999999; /* if you have YouTube iframes, is good to have a bigger z-index so the video can appear above the video */
}

/* Level 3 */
.dropDownMenu > li > ul > li > ul {
	text-align: left;
	display: none;
	background: darkcyan;
	position: absolute;
	left: 100%;
	top: 0;
	z-index: 9999999;

}

.submenustyle
{
	 background-color:#FFAB55;
	}
	 .astyle
	 {
	 	padding:10px 30px !important;
	 	color:#000000;
	 
	 	}
.nav-tabs>li.active>a,.nav-tabs>li.active>a:focus,.nav-tabs>li.active>a:hover
{
	background-color:#ff9934 !important;
	border:0px;
	margin-right:0px;
	border-radius: 0px!important;
	margin-left:0px;
	 
}

 
.nav-tabs>li>a:hover
{
	background-color:#ff9934;
 margin-right:0px;
	 border:0px;
	 border-radius: 0px!important;
	 margin-left:0px;
	
	}
	.menuhover a:hover
	{
		background-color:#FFAB55;
		margin:0px;
		border-radius: 0px!important;
		}
		.nav-tabs>li>a
		{
			border:0px;
			margin-right:0px;
			}
			.nav-tabs
			{
				border:0px;
				}
				nav-tabs>li
				{
					float:left;
					margin-bottom:0px;
					}
 
 
 



