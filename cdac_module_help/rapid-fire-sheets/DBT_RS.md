# DBT RapidSheet

1.What is normalization? What is its need? Explain 1NF , 2NF ,3NF and BCNF in detail.
-> https://beginnersbook.com/2015/05/normalization-in-dbms/



2. Explain entity relationship diagram and Explain all types of relationships with examples?

--> https://beginnersbook.com/2015/04/e-r-model-in-dbms/
 
 - ENTITY-RELATIONSHIP DIAGRAM (ERD) displays the relationships of entity set stored in a database.
   - ER diagrams help you to explain the 
     logical structure of databases.
   - ER diagram looks very similar to the 
     flowchart. However, ER Diagram includes many specialized symbols, and its meanings make this model unique.
   - ER model allows you to draw Database 
      Design.
   - It is an easy to use graphical tool for 
      modeling data.  
   - It is a GUI representation of the  
     logical   structure of a Database.  
   -  It helps you to identifies the entities  
      which exist in a system and the relationships between those
      entities.  

      <hr>
      <img src="er-notation.jpg" >
      <hr>
   - This model is based on three basic 
     concepts:
     1.Entity
      - Weak Entity
      - Strong Entity 
     2. Attribute
      - Key
      - Composite 
      - Multi-valued
      - Derived
     3. Relationship

     **Entity:**
     -  An Entity may be an object with a 
       physical existence – a particular person, car, house, or employee – or it
       may be an object with a conceptual existence – a company, a job, or a university course.
     - ***Strong Entity***
       - Strong entity set always has a primary key
       - It is represented by a rectangle symbol.
     - ***Weak Entity***    
     - Weak entity does not have enough attributes to build a primary key.
     - It is represented by a double rectangle symbol

     **Attribute**
     - Attributes are the properties which define the entity type. For example, Roll_No, Name, DOB, Age,
     Address, Mobile_No are the attributes which defines entity type Student. In ER diagram, attribute is
     represented by an oval.

     1. **Key**
     - The attribute which uniquely identifies each entity in the entity set is called key attribute.For
     example, Roll_No will be unique for each student. In ER diagram, key attribute is represented by an
     oval with underlying lines

     2. ***Composite***
     - An attribute composed of many other attribute is called as composite attribute.
     -  For example,
      Address attribute of student Entity type consists of Street, City, State, and Country. In ER diagram,
      composite attribute is represented by an oval comprising of ovals.

     3.  ***Multi-valued***
     - An attribute consisting more than one value for a given entity. For example, Phone_No (can be more
     than one for a given student). In ER diagram, multivalued attribute is represented by double oval.

     4. ***Derived***
     - An attribute which can be derived from other attributes of the entity type is known as derived
     attribute. e.g.; Age (can be derived from DOB). In ER diagram, derived attribute is represented by dashed oval.

     **Relationship**
     - A relationship type represents the association between entity types. For example,‘Enrolled in’ is a
     relationship type that exists between entity type Student and Course. In ER diagram, relationship type is
     represented by a diamond and connecting the entities with lines.

3. Which are different types of joins in SQL?
   Give examples.
-   A JOIN clause is used to fetch rows from two or more tables, based on a related column(primary key column) between them.
its mostly used to fetch data from tables which have a primary and foreign key 

- JOINs in SQL :
  1. CROSS JOIN(cartesion join)
  2. INNER JOIN
  3. OUTER JOIN
       1. LEFT OUTER JOIN
       2. RIGHT OUTER JOIN
       3. FULL OUTER JOIN
  4. SELF JOIN

  1.**CROSS JOIN**
  - Table having max record are called driving table and another is called driven table
  - CROSS JOIN join all the record of driving table to driven table
  - CROSS JOIN is without condtion.
  - It is faster than any other JOINS.

    - **SYNTAX**
    ```
    SELECT * FROM
      EMP e CROSS JOIN DEPT d;
     SELECT * FROM
     EMP e ,DEPT d;
     
     /*Both are same we can use any syntax*/

  2. **INNER JOIN**
  -  Returns the matching record from driving   and driven tables.
  - **Syntax**
  ```
   SELECT * FROM EMP e INNER JOIN DEPT d
     ON e.deptno=d.deptno;
    SELECT * FROM
     EMP e ,DEPT d
    ON e.deptno=d.deptno;
    /*Both are same we can use any syntax*/
  ```


  1. **OUTER JOIN**
  - ***LEFT OUTER JOIN:***
      Returns all records from the left table, and the matched records from the right
     table.
    - syntax
``` 
SELECT * FROM
EMP e LEFT OUTER JOIN DEPT d
ON e.deptno=d.deptno;

 SELECT * FROM
EMP e LEFT JOIN DEPT d
ON e.deptno=d.deptno;

/*Both are same we can use any syntax*/
```
  -  ***RIGHT OUTER JOIN:***
   Returns all records from the right table, and the matched records from the left
table. 
  - syntax
   > SELECT * FROM
EMP e RIGHT OUTER JOIN DEPT d
ON e.deptno=d.deptno;
   > SELECT * FROM
EMP e RIGHT JOIN DEPT d
ON e.deptno=d.deptno;

/*Both are same we can use any syntax*/

  - ***FULL OUTER JOIN:*** Returns all records when there is a match in either left or right table. - MySQL do not support FULL OUTER JOIN but set operation we can implement. - Set Operators : UNION
(Supress Duplicate Records), UNION ALL (It do not supress duplicate record)
  - syntax:
   ```
    SELECT * FROM
EMP e LEFT JOIN DEPT d
ON e.deptno=d.deptno
    UNION
SELECT * FROM
EMP e RIGHT JOIN DEPT d
ON e.deptno=d.deptno;
  ```
  - **SELF JOIN** We use JOIN on same table. 
   - syntax
```   
SELECT * FROM
EMP e INNER JOIN EMP e1
ON e.mgr=e1.empno;
SELECT * FROM
EMP e , EMP e1
WHERE e.mgr=e1.empno;
/*Both are same we can use any syntax*/
```

4.What is stored procedure? What are advantages of stored procedure?   
 -  In SQL procedure means function which do not return any value.
 - Stored Procedure getspace on server HDD and server save Stored Procedure in compiled form.
 - syntax
 ```
 DELIMITER $$
CREATE PROCEDURE sp_name (parameter(s)....)
BEGIN
/* SQL Statements...........*/
END $$
DELIMITER ;
 ```

5.How stored function is different than stored procedure?
1. - SP can return zero , single or multiple values.
   - Function must return a single value (which may be a scalar or a table).
2. - We can use transaction in SP.
   - We can't use transaction in SF.
3. -  SP can have input/output parameter.
   - Only input parameter.
4. - We can call function from SP.
   - We can't call SP from function.
5. - We can't use SP in SELECT/WHERE/HAVING statement.
   - We can use SF in SELECT/ WHERE/HAVING statement.
6. - We can use exception handling using Try-Catch block in SP.
   - We can't use Try-Catch block in SF.
7. - SP have IN/OUT/INOUT parameter.
   - SF by default IN.   

6.  What is difference between CHAR, VARCHAR and TEXT? Does data type matter while creating index?
1. - VARCHAR is variable-length 
   - CHAR is fixed length.
   - TEXT is variable-length.
2. - VARCHAR can store a maximum of 65,535 characters
   - CHAR can store a maximum of 255 characters.
   - TEXT can store maximum of 2147483647.   
3. - VARCHAR uses dynamic memory allocation.
   - CHAR uses static memory allocation.
   - TEXT uses dynamic memory allocation. 
4. - VARCHAR accepts both characters and numbers.
   - CHAR only accepts characters.
   -  TEXT accepts character,number and symbols. 
5. - VARCHAR is slower than CHAR.
   - CHAR is faster to access
   -   TEXT is slower than all.
 ```
 create table emp(name VARCHAR(20));
create table emp1(name CHAR(20));
create table emp2(name TEXT;
 ``` 
7. Explain ACID properties of transaction. What is use of transaction?’
-  A transaction in a database system must maintain Atomicity, Consistency, Isolation, and Durability −
commonly known as ACID properties − in order to ensure accuracy, completeness, and data integrity.

**ATOMICITY**
 - Operation should be performed both side of transaction or it should be rollback.

**CONSISTENCY**
-  DB should display same data.

**ISOLATION**
- Every transaction should be seperate.

**DURABILITY**
- After completion of transaction all changes should be reflect in DB.

- Transactions group a set of tasks into a single execution unit. Each transaction begins with a specific task
and ends when all the tasks in the group successfully complete. If any of the tasks fail, the transaction fails.
Therefore, a transaction has only two results: success or failure.








  

      
